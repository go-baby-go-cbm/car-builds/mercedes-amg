# User Guides

## Documents Included

1.  [Cleaning Up.pptx](Cleaning Up.pptx)
    -   Details on cleaning up after a build in a shared workspace/larger build.

2.  [Electrical Build Instructions.pptx](Electrical Build Instructions.pptx)
    -   The main instruction materials for customizing a car for Go Baby Go.
    -   Covers electrical work and general assembly.
    -   Does not touch on fitting for the child.
    -   If you only read one guide, read this one.

3.  [Fitting.pptx](Fitting.pptx)
    -   Tips and tricks for the fitting process required to customize a car for a child to use.

4.  [materials.xlsx](materials.xlsx)
    -   Complete materials list for items used in these user guides.

5.  [Sign-Off Form.pptx](Sign-Off Form.pptx)
    -   Sign-off sheet for capturing milestones and requirements in the build process.

6.  [Thank You.pptx](Thank You.pptx)
    -   Thank you note for building a Go Baby Go car.

7.  [The Plan.pptx](The Plan.pptx)
    -   Plan overview for larger build days.

8.  [Warning - Inspection Required.pptx](Warning - Inspection Required.pptx)
    -   Insert to interleve within the [Electrical Build Instructions.pptx](Electrical Build Instructions.pptx)
        for larger build days to allow for inspection and use of the [Sign-Off Form.pptx](Sign-Off Form.pptx).


## Using the Guides for Larger Build Days

A larger build day is one in which multiple volunteers come together to build as many cars as they 
are able. Often, this is 10-15 cars.

When preparing the documents for a larger build, the suggestion is to build one packet to give to each volunteer group which 
interleves all of the user guides as follows:
   -   Title page from the [Electrical Build Instructions.pptx](Electrical Build Instructions.pptx)
   -   [The Plan.pptx](The Plan.pptx)
   -   Sections A-C from the [Electrical Build Instructions.pptx](Electrical Build Instructions.pptx)
   -   [Warning - Inspection Required.pptx](Warning - Inspection Required.pptx)
   -   Sections D-M from the [Electrical Build Instructions.pptx](Electrical Build Instructions.pptx)
   -   [Warning - Inspection Required.pptx](Warning - Inspection Required.pptx)
   -   Sections N-Q from the [Electrical Build Instructions.pptx](Electrical Build Instructions.pptx)
   -   [Warning - Inspection Required.pptx](Warning - Inspection Required.pptx)
   -   [Fitting.pptx](Fitting.pptx)
   -   [Cleaning Up.pptx](Cleaning Up.pptx)
   -   [Thank You.pptx](Thank You.pptx)
   -   [Sign-Off Form.pptx](Sign-Off Form.pptx)


